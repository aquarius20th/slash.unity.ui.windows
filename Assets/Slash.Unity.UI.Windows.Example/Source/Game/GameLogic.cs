﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="GameLogic.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Slash.Unity.UI.Windows.Example.Game
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using UnityEngine;

    public class Inventory
    {
        #region Properties

        public List<InventoryItem> Items { get; set; }

        #endregion
    }

    public class Equipment
    {
        #region Constructors and Destructors

        public Equipment()
        {
            this.Slots = new Dictionary<EquipmentSlot, string>();
        }

        #endregion

        #region Properties

        public Dictionary<EquipmentSlot, string> Slots { get; private set; }

        #endregion
    }

    public enum EquipmentSlot
    {
        LeftHand,

        RightHand,
    }

    public class InventoryItem
    {
        #region Properties

        public int Amount { get; set; }

        public string TypeId { get; set; }

        #endregion
    }

    /// <summary>
    ///   This class represents our game logic which will be much bigger in a real game.
    ///   But even in a real project you will have some way to send commands into your logic (simulated by the methods of this
    ///   class) and a way to be informed about important events that occur (simulated by the events).
    /// </summary>
    public class GameLogic
    {
        #region Constructors and Destructors

        public GameLogic()
        {
            // Create initial dummy inventory.
            this.Inventory = new Inventory
            {
                Items =
                    new List<InventoryItem>
                    {
                        new InventoryItem() { TypeId = "sword", Amount = 1 },
                        new InventoryItem() { TypeId = "heal-potion", Amount = 5 },
                        new InventoryItem() { TypeId = "shield", Amount = 1 }
                    }
            };

            this.Equipment = new Equipment();
        }

        #endregion

        #region Delegates

        public delegate void EquippedDelegate();

        #endregion

        #region Events

        public event Action<EquipmentSlot, string> EquipmentSlotChanged;

        public event Action<string> InventoryItemRemoved;

        public event Action<string> InventoryItemAmountDecreased;

        public event Action<string> InventoryItemAmountIncreased;

        public event Action<string> InventoryItemAdded;

        #endregion

        #region Properties

        public Equipment Equipment { get; set; }

        public Inventory Inventory { get; private set; }

        #endregion

        #region Public Methods and Operators

        public void EquipItem(EquipmentSlot slot, string itemTypeId)
        {
            // Check if player has item in inventory.
            InventoryItem inventoryItem = this.GetInventoryItem(itemTypeId);
            if (inventoryItem == null || inventoryItem.Amount <= 0)
            {
                Debug.LogWarning("Can't equip: No item with type id '" + itemTypeId + "' in inventory.");
                return;
            }

            string currentItemTypeId;
            this.Equipment.Slots.TryGetValue(slot, out currentItemTypeId);

            // Check if same item should be equipped.
            if (currentItemTypeId == itemTypeId)
            {
                return;
            }

            // Return current equipped item.
            if (!string.IsNullOrEmpty(currentItemTypeId))
            {
                this.AddToInventory(currentItemTypeId);
            }

            // Take item from inventory.
            this.RemoveFromInventory(itemTypeId);

            // Equip new item.
            this.Equipment.Slots[slot] = itemTypeId;

            this.OnEquipmentSlotChanged(slot, itemTypeId);
        }

        public void Update(float dt)
        {
        }

        #endregion

        #region Methods

        protected virtual void OnEquipmentSlotChanged(EquipmentSlot slot, string itemTypeId)
        {
            var handler = this.EquipmentSlotChanged;
            if (handler != null)
            {
                handler(slot, itemTypeId);
            }
        }

        protected virtual void OnInventoryItemAdded(string itemTypeId)
        {
            var handler = this.InventoryItemAdded;
            if (handler != null)
            {
                handler(itemTypeId);
            }
        }

        protected virtual void OnInventoryItemAmountDecreased(string itemTypeId)
        {
            var handler = this.InventoryItemAmountDecreased;
            if (handler != null)
            {
                handler(itemTypeId);
            }
        }

        protected virtual void OnInventoryItemAmountIncreased(string itemTypeId)
        {
            var handler = this.InventoryItemAmountIncreased;
            if (handler != null)
            {
                handler(itemTypeId);
            }
        }

        protected virtual void OnInventoryItemRemoved(string itemTypeId)
        {
            var handler = this.InventoryItemRemoved;
            if (handler != null)
            {
                handler(itemTypeId);
            }
        }

        private void AddToInventory(string itemTypeId)
        {
            // Check if there is already an inventory item for the item.
            var inventoryItem = this.GetInventoryItem(itemTypeId);
            if (inventoryItem == null)
            {
                inventoryItem = new InventoryItem { TypeId = itemTypeId };
                this.Inventory.Items.Add(inventoryItem);
                this.OnInventoryItemAdded(itemTypeId);
            }

            inventoryItem.Amount += 1;
            this.OnInventoryItemAmountIncreased(itemTypeId);
        }

        private InventoryItem GetInventoryItem(string itemTypeId)
        {
            return
                this.Inventory.Items.FirstOrDefault(existingInventoryItem => existingInventoryItem.TypeId == itemTypeId);
        }

        private void RemoveFromInventory(string itemTypeId)
        {
            var inventoryItem = this.GetInventoryItem(itemTypeId);
            if (inventoryItem == null)
            {
                return;
            }

            inventoryItem.Amount -= 1;
            this.OnInventoryItemAmountDecreased(itemTypeId);

            // Remove completely if zero.
            if (inventoryItem.Amount == 0)
            {
                this.Inventory.Items.Remove(inventoryItem);
                this.OnInventoryItemRemoved(itemTypeId);
            }
        }

        #endregion
    }
}