﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ExampleMain.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Slash.Unity.UI.Windows.Example
{
    using UnityEngine;

    public class ExampleMain : MonoBehaviour
    {
        #region Fields

        public string MainWindowId = "Main";

        #endregion

        #region Methods

        protected void Start()
        {
            // Load main window.
            WindowManager.Instance.OpenWindow(this.MainWindowId);
        }

        #endregion
    }
}