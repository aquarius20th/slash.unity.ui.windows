﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="InventoryContext.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Slash.Unity.UI.Windows.Example.Inventory
{
    using Slash.Unity.DataBind.Core.Data;

    public class InventoryContext : Context
    {
        #region Fields

        private readonly Property<Collection<InventoryItemContext>> slotsProperty =
            new Property<Collection<InventoryItemContext>>(new Collection<InventoryItemContext>());

        #endregion

        #region Properties

        public Collection<InventoryItemContext> Slots
        {
            get
            {
                return this.slotsProperty.Value;
            }
            set
            {
                this.slotsProperty.Value = value;
            }
        }

        #endregion
    }
}