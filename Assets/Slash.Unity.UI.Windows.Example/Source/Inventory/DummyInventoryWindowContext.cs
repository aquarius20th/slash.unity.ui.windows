﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DummyInventoryWindowContext.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Slash.Unity.UI.Windows.Example.Inventory
{
    using Slash.Unity.DataBind.Core.Data;
    using Slash.Unity.UI.Windows.Example.Game;
    using Slash.Unity.UI.Windows.Example.Inventory.DragDrop;

    public class DummyInventoryWindowContext : InventoryWindowContext
    {
        #region Constructors and Destructors

        public DummyInventoryWindowContext()
        {
            const string SwordTypeId = "sword";
            this.Inventory = new InventoryContext()
            {
                Slots =
                    new Collection<InventoryItemContext>()
                    {
                        new InventoryItemContext() { TypeId = SwordTypeId, Amount = 1 },
                        new InventoryItemContext() { TypeId = "heal-potion", Amount = 5 }
                    }
            };
            var leftHandSlot = new EquipmentSlotContext() { Slot = EquipmentSlot.LeftHand, TypeId = SwordTypeId };
            var rightHandSlot = new EquipmentSlotContext() { Slot = EquipmentSlot.RightHand };
            this.Equipment = new EquipmentContext() { LeftHandSlot = leftHandSlot, RightHandSlot = rightHandSlot };

            this.Drop += this.OnSlotDrop;
        }

        #endregion

        #region Methods

        private void OnSlotDrop(EquipmentSlotContext slot, ItemDragData itemDragData)
        {
            slot.TypeId = itemDragData.Item.TypeId;
            itemDragData.Item.Amount -= 1;
        }

        #endregion
    }
}