﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="InventoryWindowContext.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Slash.Unity.UI.Windows.Example.Inventory
{
    using System;

    using Slash.Unity.DataBind.Core.Data;
    using Slash.Unity.Input.DragDrop;
    using Slash.Unity.UI.Windows.Example.Inventory.DragDrop;

    public class InventoryWindowContext : Context
    {
        #region Fields

        private readonly Property<EquipmentContext> equipmentProperty = new Property<EquipmentContext>();

        private readonly Property<InventoryContext> inventoryProperty = new Property<InventoryContext>();

        #endregion

        #region Events

        public event Action Close;

        public event Action<EquipmentSlotContext, ItemDragData> Drop;

        #endregion

        #region Properties

        public EquipmentContext Equipment
        {
            get
            {
                return this.equipmentProperty.Value;
            }
            set
            {
                this.equipmentProperty.Value = value;
            }
        }

        public InventoryContext Inventory
        {
            get
            {
                return this.inventoryProperty.Value;
            }
            set
            {
                this.inventoryProperty.Value = value;
            }
        }

        #endregion

        #region Public Methods and Operators

        public void DoClose()
        {
            this.OnClose();
        }

        public void DoDrop(DragDropOperation dragDropOperation, EquipmentSlotContext slot)
        {
            if (dragDropOperation == null)
            {
                return;
            }

            var itemDragData = dragDropOperation.Data as ItemDragData;
            if (itemDragData == null)
            {
                dragDropOperation.DropSuccessful = false;
                return;
            }

            this.OnDrop(slot, itemDragData);
            dragDropOperation.DropSuccessful = true;
        }

        #endregion

        #region Methods

        private void OnClose()
        {
            var handler = this.Close;
            if (handler != null)
            {
                handler();
            }
        }

        private void OnDrop(EquipmentSlotContext slot, ItemDragData dragData)
        {
            var handler = this.Drop;
            if (handler != null)
            {
                handler(slot, dragData);
            }
        }

        #endregion
    }
}