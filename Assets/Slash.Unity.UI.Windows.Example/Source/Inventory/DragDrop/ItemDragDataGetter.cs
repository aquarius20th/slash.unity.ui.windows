﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ItemDragDataGetter.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Slash.Unity.UI.Windows.Example.Inventory.DragDrop
{
    using Slash.Unity.DataBind.Core.Presentation;
    using Slash.Unity.Input.DragDrop;

    using UnityEngine;

    public class ItemDragDataGetter : MonoBehaviour
    {
        #region Fields

        public ContextHolder ContextHolder;

        #endregion

        #region Public Methods and Operators

        public void GetDragData(DragDropOperation operation)
        {
            operation.Data = new ItemDragData() { Item = this.ContextHolder.Context as InventoryItemContext };
        }

        #endregion

        #region Methods

        protected void Reset()
        {
            if (this.ContextHolder == null)
            {
                this.ContextHolder = this.GetComponent<ContextHolder>();
            }
        }

        #endregion
    }
}